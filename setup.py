from rage import __version__
from setuptools import setup

setup(
    name='rage',
    version=__version__,
    description='Safely and automatically migrate database schemas',
    author='Metroleads Team',
    author_email='metroleads.dev@metroleads.com',
    entry_points={'console_scripts': ['rage = rage.core:main']},
    packages=['rage'],
)
