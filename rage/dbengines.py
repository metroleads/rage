import collections
import logging
import random
import sqlite3
import string
import os
import re

try:
    import json
except ImportError:
    import simplejson as json
import subprocess

logger = logging.getLogger(__name__)


loads_string_keys = lambda s: dict(
    (str(k), v) for k, v in json.loads(s).items()
)


class SQLException(Exception):
    pass


FilenameSha1 = collections.namedtuple('FilenameSha1', 'filename sha1')


class DatabaseMigrationEngine(object):
    migration_table_sql = (
        "CREATE TABLE dbmigration "
        "(filename varchar(255), sha1 varchar(40), date datetime);")

    def create_migration_table(self):
        self.execute(self.migration_table_sql)

    def sql(self, directory, files_sha1s_to_run):
        for filename, sha1 in sorted(files_sha1s_to_run):
            command = None
            sql_statements = []
            sql_statements.append(
                '-- start filename: %s sha1: %s' % (filename, sha1))
            if os.path.splitext(filename)[-1] == '.sql':
                sql_statements += open(
                    os.path.join(directory, filename)).read().splitlines()
            else:
                command = os.path.join(directory, filename)
            sql_statements.append(
                "INSERT INTO dbmigration (filename, sha1, date) "
                "VALUES ('%s', '%s', %s());" %
                (filename, sha1, self.date_func))
            yield command, "\n".join(sql_statements)

    def performed_migrations(self):
        return [FilenameSha1(r[0], r[1]) for r in self.results(
            "SELECT filename, sha1 FROM dbmigration ORDER BY filename")]

class cassandra(DatabaseMigrationEngine):
    """a migration engine for cassandra"""

    date_func = 'datetime'

    def __init__(self, connection_string, keyspace):
        self.cassandra_cli_path = subprocess.check_output(['which', 'cassandra-cli']).strip()
        self.connection_string = connection_string if connection_string else "localhost"
        self.keyspace = keyspace

    def run_direct_command(self, statements):
        file_name = '/tmp/' + ''.join([random.choice(string.ascii_letters) for x in range(5)]) + '.cli'
        print 'Generated temporary file to execute statements: %s' % file_name
        tmp_file = open(file_name, "w")
        tmp_file.writelines(statements)
        tmp_file.close()
        try:
            return self.run_script(file_name)
        finally:
            os.remove(file_name)

    def run_script(self, file_path):
        params = [self.cassandra_cli_path, "-h", self.connection_string, "-f", file_path]
        print 'Running script with params: %s' % ' '.join(params)
        return subprocess.check_output(params, stderr=subprocess.STDOUT)

    def create_migration_table(self):
        exists_statements = ['USE %s;\r\nLIST rage_migrations;\r\nexit;\r\n' % self.keyspace]
        try:
            op = self.run_direct_command(exists_statements)
        except subprocess.CalledProcessError as cpe:
            op = cpe.output

        if re.search('rage_migrations not found in current keyspace', op, re.MULTILINE):
            create_statements = ['USE %s;\r\nCREATE COLUMN FAMILY rage_migrations with key_validation_class = UTF8Type '
                                 'and default_validation_class = UTF8Type '
                                 'and comparator = UTF8Type;' % self.keyspace]
            op = self.run_direct_command(create_statements)
            print 'Migration column family successfully created'
        else:
            print 'Migration column family already exists. Will not do anything'

    def execute_statement(self, statements):
        print 'Executing statements: %s' % statements
        op = self.run_direct_command(statements)

    def execute_script(self, script_file_path):
        print 'Executing CLI at: %s' % script_file_path
        op = self.run_script(script_file_path)

    def sql(self, directory, files_sha1s_to_run):
        for filename, sha1 in sorted(files_sha1s_to_run):
            command = os.path.join(directory, filename)
            sql_statements = []
            sql_statements.append(
                "USE %s;\r\nSET rage_migrations['all']['%s'] = '%s';\r\nexit;\r\n" %
                (self.keyspace, filename, sha1))
            yield command, "\n".join(sql_statements)

    def performed_migrations(self):
        print 'Fetching existing migrations...'
        fetch_statements = ['USE %s;\r\nlist rage_migrations;\r\nexit;\r\n' % self.keyspace]
        op = self.run_direct_command(fetch_statements)

        # Support for 1.2.9+ and before
        regex_matches = [r for r in re.findall('\(column=(.*), value=(.*),', op)] or \
                        [r for r in re.findall('\(name=(.*), value=(.*),', op)] or \
                        []

        migrations_found = [FilenameSha1(r[0], r[1]) for r in regex_matches]

        print 'Migrations found: %s' % migrations_found
        return migrations_found

class sqlite(DatabaseMigrationEngine):
    """a migration engine for sqlite"""
    date_func = 'datetime'

    def __init__(self, connection_string):
        self.connection = sqlite3.connect(connection_string)

    def execute(self, statement):
        try:
            return self.connection.executescript(statement)
        except sqlite3.OperationalError as e:
            raise SQLException(str(e))

    def results(self, statement):
        try:
            return self.connection.execute(statement).fetchall()
        except sqlite3.OperationalError as e:
            raise SQLException(str(e))


class GenericEngine(DatabaseMigrationEngine):
    """a generic database engine"""
    date_func = 'now'

    def __init__(self, connection_string):
        self.connection = self.engine.connect(
            **loads_string_keys(connection_string)
        )
        self.ProgrammingError = self.engine.ProgrammingError
        self.OperationalError = self.engine.OperationalError

    def execute(self, statement):
        try:
            c = self.connection.cursor()
            c.execute(statement)
            return c
        except (self.ProgrammingError, self.OperationalError) as e:
            self.connection.rollback()
            raise SQLException(str(e))

    def results(self, statement):
        return list(self.execute(statement).fetchall())


class mysql(GenericEngine):
    """a migration engine for mysql"""

    def __init__(self, connection_string):
        import MySQLdb
        self.engine = MySQLdb
        super(mysql, self).__init__(connection_string)


class postgres(GenericEngine):
    """a migration engine for postgres"""

    migration_table_sql = (
        "CREATE TABLE dbmigration "
        "(filename varchar(255), sha1 varchar(40), date timestamp);")

    def __init__(self, connection_string):
        import psycopg2
        self.engine = psycopg2
        connection_dict = json.loads(connection_string)
        schema = connection_dict.pop('schema', None)
        super(postgres, self).__init__(json.dumps(connection_dict))
        if schema:
            self.execute('SET search_path = %s' % schema)

    def execute(self, statement):
        try:
            c = self.connection.cursor()
            c.execute(statement)
            self.connection.commit()
            return c
        except (self.ProgrammingError, self.OperationalError) as e:
            self.connection.rollback()
            raise SQLException(str(e))
